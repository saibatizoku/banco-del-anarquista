# El Banco de Carpintería del Anarquista

Esta es una traducción del libro "The Anarchist's Workbench" de
Christopher Schwarz, editado por Lost Art Press -
https://lostartpress.com/collections/books/products/the-anarchists-workbench

La traducción, al igual que el original, está bajo una licencia
Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0).
