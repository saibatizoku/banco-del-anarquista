![](./ilustraciones/prologo-01-ductile-iron-holdfast.png)

(FIXME - traducir pie de foto)
# Prólogo

A primera vista, el banco de carpintería de este libro parece ser casi
idéntico al banco que construi en 2005, que ha aparecido en varias
revistas y libros.  Es bastante robusto, hecho de pino amarillo y para
sostener el trabajo tiene una prensa en una pata, un tope para
cepillar y "holdfasts" (FIXME - holdfasts).  Aun cuando son muy
similares, el diseño del banco de este libro tiene muchas mejorías.
Durante los últimos 15 años he encontrado formas mejores de laminar la
superficie y usar menos sargentos, formas más fáciles de cortar los
ensambles gigantescos, y trucos para marcar y medir que tienen como
resultado ensambles más exactos.  La superficie es más gruesa, más
pesada y genera menos desechos cuando se usa madera pre-cortada de 2x12.

Las formas de sostener el trabajo en el banco funcionan mucho mejor
ahora.  Gracias a la evolución de la manufactura de las prensas, y un
mejor entendimiento de cómo funcionan, la prensa en una pata es
suficientemente fuerte como para sostener tablas sin la ayuda de un
"sliding deadman" (FIXME - sliding deadman).  No hay guía paralela,
entonces puedes trabajar sobre la prensa sin agacharte.  El tope para
cepillar tiene dientes de metal, hechos por un herrero, que sostienen
la madera sin que se deslice tanto.  Y el arreglo de agujeros para
"holdfasts" sobre la superficie - algo que me tomó años perfeccionar -
garantiza que casi siempre habrá un agujero justo donde lo necesitas.

El que este banco sea similar al que hice en 2005 es algo que me
tranquiliza.  Significa que no estaba yo tan mal cuando comencé este
camino.  Es igual de importante hacer resaltar que después de 15 años
de construir bancos de carpintería de muchas formas diferentes, desde
bancos romanos antiguos hasta uno miniatura de Dinamarca, no se ha
cambiado mi idea de que un banco sencillo con ensambles grandes es lo
ideal para muchos carpinteros.


In addition to the fully matured workbench design, this book also
dives a little deeper into the past to explore the origins of this form. I
first encountered this type of bench in a French book from about 1774,
and at the time I couldn’t find much else written about it. Since then,
libraries and museums have digitized their collections and opened
them to the public. So we’ve been able to trace its origins back anoth-
er 200 years and found evidence it emerged somewhere in the Low
Countries or northern France in the 1500s. We also have little doubt
there are more discoveries to be made.

And finally, the story of this bench is deeply intertwined with my
own story as a woodworker, researcher, publisher and – of course –
aesthetic anarchist.

That’s why I’ve decided to give away the content of this book to the
world at large. The electronic version of the book is free to download,
reproduce and give away to friends. You can excerpt chapters for your
woodworking club. Print it all out, bind it and give it away as a gift.
The only thing you cannot do is sell it or make money off of it in any
way. Put simply: Commercial use of this material is strictly prohibit-
ed. But other than that, go nuts.

If you prefer a nicely bound book instead of an electronic copy, we
sympathize. That’s what we prefer, too. So we are printing some copies
of this book for people who prefer it in that format. Those will cost a
bit of money to make (we don’t make low-quality crap here at Lost Art
Press) so we won’t be able to give those away. But we will sell them – as
always – at a fair price for a book that is printed in the United States,
sewn, bound in fiber tape and covered in a durable hardback.
This book is the final chapter in the “anarchist” series – “The Anar-
chist’s Tool Chest,” “The Anarchist’s Design Book” and now “The
Anarchist’s Workbench.” And it is (I hope) my last book on work-
benches. So it seemed fitting that to thank all the woodworkers who
have supported me during this journey, this book should belong to
everyone.
<br />
<br />
<br />
Christopher Schwarz<br />
June 2020<br />
Covington, Kentucky


![](./ilustraciones/prologo-02-antique-vise-screws.png)

(FIXME - traducir pie de foto)

![](./ilustraciones/prologo-03-covington-workshop-2016.png)

(FIXME - traducir pie de foto)
